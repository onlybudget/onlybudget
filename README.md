# Only Budget #

Only Budget is an android application for tracking spending habits.

## What makes us different? ##

Only Budget trusts users to categorize what they do in an honest and efficient manor.
We don't integrate with your accounts, we upload no information from your device, and we respect your privacy.

Feature wise, the primary difference in Only Budget is the acknowledgement that the *category* of an expense is unique from whether or not it is a responsible purchase or a frivolous one.
You can splurge on groceries (cashews, candy bars, etc.) and you can make important purchases on alcohol (buying your boss a drink).

## License ##

The app is free!
Don't modify our code, and do not use it for anything other than compiling the application as it exists.

We'll figure out a license later.  
