package edu.wwu.onlybudget.util;

public class MathExt {
    public static <T extends Comparable<T>> T clamp(final T value, final T min, final T max) {
        if (value.compareTo(min) < 0) {
            return min;
        } else if (value.compareTo(max) > 0) {
            return max;
        } else {
            return value;
        }
    }
}
