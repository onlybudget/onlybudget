package edu.wwu.onlybudget.util;

public final class Cubehelix {
    private static final double DEG_TO_RAD = Math.PI / 180.0;
    private static final double GAMMA = 1;

    private static final HSL zero = new HSL(-100, 0.75, 0.35);
    private static final HSL half = new HSL(80, 1.5, 0.8);
    private static final HSL one = new HSL(260, 0.75, 0.35);

    public static int interpolate(double time) {
        final HSL from, to;
        if (time < 0.5) {
            from = zero;
            to = half;
            time *= 2;
        } else {
            from = half;
            to = one;
            time = (time - 0.5) * 2;
        }
        final double ah = (from.hue + 120) * DEG_TO_RAD;
        final double bh = (to.hue + 120) * DEG_TO_RAD - ah;
        final double as = from.saturation;
        final double bs = to.saturation - as;
        final double al = from.lightness;
        final double bl = to.lightness - al;

        final double h = ah + bh * time;
        final double l = Math.pow(al + bl * time, GAMMA);
        final double a = (as + bs * time) * l * (1 - l);

        return 0xFF000000
                | (toByte(l + a * (-0.14861 * Math.cos(h) + 1.78277 * Math.sin(h))) << 16)
                | (toByte(l + a * (-0.29227 * Math.cos(h) - 0.90649 * Math.sin(h))) << 8)
                | (toByte(l + a * (+1.97294 * Math.cos(h))));
    }

    private static int toByte(final double value) {
        return (int) (256 * MathExt.clamp(value, 0.0, 0.9999));
    }
}
