package edu.wwu.onlybudget.util;

public class HSL {
    public final double hue;
    public final double saturation;
    public final double lightness;

    public HSL(final double hue, final double saturation, final double lightness) {
        this.hue = hue;
        this.saturation = saturation;
        this.lightness = lightness;
    }
}
