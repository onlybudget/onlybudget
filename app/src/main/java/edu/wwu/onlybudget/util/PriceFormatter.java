package edu.wwu.onlybudget.util;

import java.util.Locale;

public final class PriceFormatter {
    public static String format(final double value) {
        return String.format(Locale.getDefault(), "$%.2f", value);
    }
}
