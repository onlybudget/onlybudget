package edu.wwu.onlybudget.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.Date;
import java.util.Date;
import java.util.Iterator;

import edu.wwu.onlybudget.model.Category;
import edu.wwu.onlybudget.model.Expense;

public class DatabaseManager extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "onlyBudgetDB";
    private static final int DATABASE_VERSION = 1;

    public DatabaseManager(final Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(final SQLiteDatabase db) {
        Expense.handle.createTableIfNotExists(db);
        Category.handle.createTableIfNotExists(db);
    }

    public void onUpgrade(final SQLiteDatabase db, int oldVersion, int newVersion) {
        // HACK: Probably figure out actual upgrades.
        onCreate(db);
    }

    public <T> T store(final SQLiteDatabaseItem<T> item) {
        return item.update(getWritableDatabase());
    }

    private <T> T load(final SQLiteDatabaseItem<T> item) {
        return item.load(getReadableDatabase());
    }

    public Expense loadExpense(final long id) {
        return load(new Expense(id));
    }

    public Iterable<Expense> loadAllExpenses() {
        return Expense.loadMany(getReadableDatabase());
    }

    public Category loadCategory(final long id) {
        return load(new Category(id));
    }
}
