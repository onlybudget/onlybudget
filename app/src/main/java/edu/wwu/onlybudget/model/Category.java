package edu.wwu.onlybudget.model;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteStatement;

import java.util.ArrayList;

public final class Category extends SQLiteDatabaseItem<Category> {
    private String name;
    private boolean deleted;

    public static Category handle = new Category();

    public Category() {
        this(-1);
    }

    public Category(String name) {
        this();
        this.name = name;
    }

    public Category(final long id) {
        this(id, null, false);
    }

    public Category(final long id, final String name, final boolean deleted) {
        super(id);
        this.name = name;
        this.deleted = deleted;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (!deleted) {
            this.name = name;
        }
    }

    public boolean isDeleted() {
        return deleted;
    }

    @Override
    public boolean createTableIfNotExists(final SQLiteDatabase database) {
        try {
            Cursor result = database.rawQuery(SQLiteStrings.TABLE_EXISTS, new String[0]);
            if (result.getCount() == 0) {
                database.execSQL(SQLiteStrings.CREATE_TABLE_IF_NOT_EXISTS);
                // default categories
                new Category(1, "Groceries", false).update(database);
                new Category(2, "Alcohol", false).update(database);
                new Category(3, "Bills & Utilities", false).update(database);
                new Category(4, "Video Games", false).update(database);
                new Category(5, "Debts", false).update(database);
                new Category(6, "Charity", false).update(database);
            }
            return true;
        } catch (final SQLiteException e) {
            return false;
        }
    }

    private void assignFrom(final Cursor cursor) {
        id = cursor.getLong(0);
        name = cursor.getString(1);
        deleted = (1 == cursor.getLong(2));
    }

    @Override
    public Category load(final SQLiteDatabase database) {
        if (!createTableIfNotExists(database)) {
            return null;
        }
        try {
            final Cursor result = database.rawQuery(
                    SQLiteStrings.GET,
                    new String[]{Long.toString(id)});
            if (result.moveToFirst()) {
                this.assignFrom(result);
                return this;
            } else {
                return new Category("");
            }
        } catch (final SQLiteException e) {
            return null;
        }
    }

    public static Iterable<Category> loadMany(final SQLiteDatabase database) {
        try {
            final Cursor cursor = database.rawQuery(SQLiteStrings.GET_ALL, new String[0]);
            final ArrayList<Category> results = new ArrayList<>();

            while (cursor.moveToNext()) {
                final Category next = new Category();
                next.assignFrom(cursor);
                results.add(next);
            }

            return results.size() != 0
                    ? results
                    : null;
        } catch (final SQLiteException e) {
            return null;
        }
    }

    @Override
    public Category update(final SQLiteDatabase database) {
        if (!createTableIfNotExists(database)) {
            return null;
        }
        try {
            final boolean isInsert = id == -1;
            final String sql = isInsert ? SQLiteStrings.INSERT : SQLiteStrings.REPLACE;
            final SQLiteStatement statement = database.compileStatement(sql);
            int column = 1;
            if (!isInsert) {
                statement.bindLong(column++, id);
            }
            statement.bindString(column++, name);
            statement.bindLong(column, deleted ? 1 : 0);

            if (isInsert) {
                id = statement.executeInsert();
            } else {
                statement.execute();
            }
            return this;
        } catch (final SQLiteException e) {
            return null;
        }
    }

    @Override
    public boolean delete(final SQLiteDatabase database) {
        if (!createTableIfNotExists(database)) {
            return false;
        }
        try {
            final SQLiteStatement statement = database.compileStatement(SQLiteStrings.DELETE);
            statement.bindLong(1, this.id);
            final int numDeletions = statement.executeUpdateDelete();
            return numDeletions == 1;
        } catch (final SQLiteException e) {
            return false;
        }
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", deleted=" + deleted +
                '}';
    }

    private static final class SQLiteStrings {
        private static final String CREATE_TABLE_IF_NOT_EXISTS =
                "CREATE TABLE IF NOT EXISTS Category (\n" +
                        "Id      INTEGER PRIMARY KEY,\n" +
                        "Name    TEXT,\n" +
                        "Deleted INTEGER);";

        private static final String TABLE_EXISTS =
                "SELECT name FROM sqlite_master WHERE type='table' AND name='Category'";

        private static final String GET =
                "SELECT *\n" +
                        "FROM Category\n" +
                        "WHERE Id = ?\n" +
                        "LIMIT 1;";

        private static final String INSERT =
                "INSERT INTO\n" +
                        "Category (Name, Deleted)\n" +
                        "VALUES  (?, ?);";

        private static final String REPLACE =
                "REPLACE INTO\n" +
                        "Category (Id, Name, Deleted)\n" +
                        "VALUES   (?, ?, ?);";

        private static final String DELETE =
                "UPDATE Category SET Deleted = 1 WHERE Id = ?;";

        private static final String GET_ALL =
                "SELECT * FROM Category;";
    }
}
