package edu.wwu.onlybudget.model;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteStatement;

import java.util.ArrayList;
import java.util.Date;

public final class Expense extends SQLiteDatabaseItem<Expense> {
    private long categoryId;
    private String description;
    private double amount;
    private double frivolous;
    private double tax;
    private Date date;

    public static final Expense handle = new Expense();

    public Expense() {
        this(-1L);
    }

    public Expense(long id) {
        this(id, -1L, null, 0.0, 0.0, 0.0, null);
    }

    public Expense(long id, long categoryId, String description, double amount, double frivolous, double tax, Date date) {
        super(id);
        setCategoryId(categoryId);
        setDescription(description);
        setAmount(amount);
        setFrivolous(frivolous);
        setTax(tax);
        setDate(date);
    }

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(final long categoryId) {
        this.categoryId = categoryId;
    }

    public String getDescription() {
        if (description == null) {
            setDescription(null);
        }
        return description;
    }

    public void setDescription(final String description) {
        this.description =
                description != null ? description : "";
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(final double amount) {
        this.amount = amount;
    }

    public double getFrivolous() {
        return frivolous;
    }

    public void setFrivolous(final double frivolous) {
        this.frivolous = frivolous;
    }

    public double getTax() {
        return tax;
    }

    public void setTax(final double tax) {
        this.tax = tax;
    }

    public Date getDate() {
        if (date == null) {
            setDate(null);
        }
        return date;
    }

    public void setDate(final Date date) {
        this.date =
                date != null ? date : new Date();
    }

    @Override
    public boolean createTableIfNotExists(final SQLiteDatabase database) {
        try {
            database.execSQL(SQLiteStrings.CREATE_TABLE_IF_NOT_EXISTS);
            return true;
        } catch (final SQLiteException e) {
            return false;
        }
    }

    private void assignFrom(final Cursor cursor) {
        id = cursor.getLong(0);
        categoryId = cursor.getLong(1);
        description = cursor.getString(2);
        amount = cursor.getDouble(3);
        frivolous = cursor.getDouble(4);
        tax = cursor.getDouble(5);
        date = new Date(cursor.getLong(6));
    }

    @Override
    public Expense load(final SQLiteDatabase database) {
        if (!createTableIfNotExists(database)) {
            return null;
        }
        try {
            final Cursor result = database.rawQuery(
                    SQLiteStrings.GET,
                    new String[]{Long.toString(id)});
            if (result.moveToFirst()) {
                this.assignFrom(result);
                return this;
            } else {
                return null;
            }
        } catch (final SQLiteException e) {
            return null;
        }
    }

    public static Iterable<Expense> loadMany(final SQLiteDatabase database) {
        try {
            final Cursor cursor = database.rawQuery(SQLiteStrings.GET_ALL, new String[0]);
            final ArrayList<Expense> results = new ArrayList<>();

            while (cursor.moveToNext()) {
                final Expense next = new Expense();
                next.assignFrom(cursor);
                results.add(next);
            }

            return results.size() != 0
                    ? results
                    : null;
        } catch (final SQLiteException e) {
            return null;
        }
    }

    @Override
    public Expense update(final SQLiteDatabase database) {
        if (!createTableIfNotExists(database)) {
            return null;
        }
        try {
            final boolean isInsert = id == -1;
            final String sql = isInsert ? SQLiteStrings.INSERT : SQLiteStrings.REPLACE;
            final SQLiteStatement statement = database.compileStatement(sql);
            int column = 1;
            if (!isInsert) {
                statement.bindLong(column++, id);
            }
            statement.bindLong(column++, categoryId);
            statement.bindString(column++, getDescription());
            statement.bindDouble(column++, amount);
            statement.bindDouble(column++, frivolous);
            statement.bindDouble(column++, tax);
            statement.bindLong(column, getDate().getTime());
            if (isInsert) {
                id = statement.executeInsert();
            } else {
                statement.execute();
            }
            return this;
        } catch (final SQLiteException e) {
            return null;
        }
    }

    @Override
    public boolean delete(final SQLiteDatabase database) {
        if (!createTableIfNotExists(database)) {
            return false;
        }
        try {
            final SQLiteStatement statement = database.compileStatement(SQLiteStrings.DELETE);
            statement.bindLong(1, this.id);
            final int numDeletions = statement.executeUpdateDelete();
            return numDeletions == 1;
        } catch (final SQLiteException e) {
            return false;
        }
    }

    @Override
    public String toString() {
        return "Expense{" +
                "id=" + id +
                ", categoryId=" + categoryId +
                ", description='" + description + '\'' +
                ", amount=" + amount +
                ", frivolous=" + frivolous +
                ", tax=" + tax +
                ", date=" + date +
                '}';
    }

    private static final class SQLiteStrings {
        private static final String CREATE_TABLE_IF_NOT_EXISTS =
                "CREATE TABLE IF NOT EXISTS Expense (\n" +
                        "Id          INTEGER PRIMARY KEY,\n" +
                        "CategoryId  INTEGER,\n" +
                        "Description TEXT,\n" +
                        "Amount      REAL,\n" +
                        "Frivolous   REAL,\n" +
                        "Tax         REAL,\n" +
                        "Date        INTEGER);";

        private static final String GET =
                "SELECT *\n" +
                        "FROM Expense\n" +
                        "WHERE Id = ?\n" +
                        "LIMIT 1;";

        private static final String GET_ALL =
                "SELECT * FROM Expense;";

        private static final String INSERT =
                "INSERT INTO\n" +
                        "Expense (CategoryId, Description, Amount, Frivolous, Tax, Date)\n" +
                        "VALUES  (?, ?, ?, ?, ?, ?);";

        private static final String REPLACE =
                "REPLACE INTO\n" +
                        "Expense (Id, CategoryId, Description, Amount, Frivolous, Tax, Date)\n" +
                        "VALUES  (?, ?, ?, ?, ?, ?, ?);";

        private static final String DELETE =
                "DELETE FROM Expense WHERE ID = ? LIMIT 1;";
    }
}
