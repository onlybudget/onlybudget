package edu.wwu.onlybudget.model;

import android.content.Context;

public class ExpenseButton extends android.support.v7.widget.AppCompatButton {

    private Expense expense;

    public ExpenseButton(Context context, Expense newExpense) {
        super (context);
        expense = newExpense;
    }

    public String getDescription() { return expense.getDescription(); }

    public double getCategory() { return expense.getCategoryId(); }

    public double getTotal() { return expense.getAmount(); }

    public double getFriv() { return expense.getFrivolous(); }
}
