package edu.wwu.onlybudget.model;

import android.database.sqlite.SQLiteDatabase;

public abstract class SQLiteDatabaseItem<T> {
    protected long id;

    public SQLiteDatabaseItem(final long id) {
        this.id = id;
    }

    /**
     * Gets the primary key associated with this object.
     *
     * @return The id of the object.
     */
    public long getId() {
        return id;
    }

    /**
     * Guarantees that the database contains the table for objects of this type.
     *
     * @param database The database.
     * @return true if the database exists after the call is made, false otherwise.
     */
    public abstract boolean createTableIfNotExists(SQLiteDatabase database);

    /**
     * Reloads the value indicated by this primary key, updating the values of `this` object.
     *
     * @param database The database.
     * @return `this` on success, null otherwise.
     */
    public abstract T load(SQLiteDatabase database);

    /**
     * Stores the value, either inserting or replacing, in the database.
     *
     * @param database The database.
     * @return `this` on success, null otherwise.
     */
    public abstract T update(SQLiteDatabase database);

    /**
     * Deletes the value from the database.
     * This sets the primary key to the not-stored value.
     *
     * @param database The database.
     * @return Returns true if successful, false otherwise.
     */
    public abstract boolean delete(SQLiteDatabase database);
}
