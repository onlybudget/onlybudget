package edu.wwu.onlybudget.view;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import edu.wwu.onlybudget.R;
import edu.wwu.onlybudget.model.Category;
import edu.wwu.onlybudget.model.DatabaseManager;
import edu.wwu.onlybudget.model.Expense;
import edu.wwu.onlybudget.util.PriceFormatter;

public class ExpensesListActivity extends AppCompatActivity {
    private ScrollView scrollView;
    private DatabaseManager dbManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expenseslist);

        dbManager = new DatabaseManager(this);
        scrollView = findViewById(R.id.scrollView);
        updateView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_expenses, menu);
        return true;
    }

    public void updateView() {
        final Iterable<Expense> expenses = dbManager.loadAllExpenses();

        if (expenses != null) {
            scrollView.removeAllViewsInLayout();

            final LinearLayout verticalLayout = new LinearLayout(this);
            verticalLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            verticalLayout.setOrientation(LinearLayout.VERTICAL);

            for (final Expense expense : expenses) {
                final Category category = dbManager.loadCategory(expense.getCategoryId());

                final View card = LayoutInflater.from(this)
                        .inflate(R.layout.card_expense_list, null);
                card.setOnClickListener(new ExpenseClickedListener(expense.getId()));

                {
                    TextView tv;

                    tv = card.findViewById(R.id.display_expense_name);
                    tv.setText(expense.getDescription());

                    tv = card.findViewById(R.id.display_category_name);
                    tv.setText(category.getName());

                    tv = card.findViewById(R.id.display_total_amount);
                    tv.setText(PriceFormatter.format(expense.getAmount()));

                    tv = card.findViewById(R.id.display_frivolous_amount);
                    if (expense.getFrivolous() == 0) {
                        tv.setVisibility(View.INVISIBLE);
                    } else {
                        tv.setText(PriceFormatter.format(expense.getFrivolous()));
                    }
                }

                verticalLayout.addView(card);
            }
            scrollView.addView(verticalLayout);

        } else {
            scrollView.removeAllViewsInLayout();
            Toast.makeText(ExpensesListActivity.this, "No Expenses To List", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        updateView();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {

            case R.id.action_settings:
                //Intent insertIntent = new Intent(this, SettingsActivity.class);
                Intent insertIntent = new Intent(this, CategoryListActivity.class);
                this.startActivity(insertIntent);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private class ExpenseClickedListener implements View.OnClickListener {
        private final long id;

        public ExpenseClickedListener(final long id) {
            this.id = id;
        }

        public void onClick(View v) {
            final Intent intent = new Intent(ExpensesListActivity.this, ExpenseActivity.class);
            intent.putExtra(ExpenseActivity.INITIAL_EXPENSE_ID, id);
            startActivityForResult(intent, 0);
        }
    }

    public void ExpenseActivityView(View v) {
        Intent intent = new Intent(this, ExpenseActivity.class);
        this.startActivityForResult(intent, 0);
    }

    public void SummaryActivityView(View v) {
        final Iterable<Expense> expenses = dbManager.loadAllExpenses();
        if (expenses != null) {
            Intent intent = new Intent(this, SummaryActivity.class);
            this.startActivity(intent);
        } else {
            Toast.makeText(ExpensesListActivity.this, "No Expenses to Summarize", Toast.LENGTH_LONG).show();
        }
    }
}
