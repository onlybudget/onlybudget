package edu.wwu.onlybudget.view;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public final class DefaultDateFormatter {
    private static SimpleDateFormat instance = new SimpleDateFormat("MM/dd/yyyy", Locale.getDefault());

    public static String format(Date d) {
        return instance.format(d);
    }

    public static String format(Calendar c) {
        return format(c.getTime());
    }
}
