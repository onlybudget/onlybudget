package edu.wwu.onlybudget.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;


import edu.wwu.onlybudget.R;
import edu.wwu.onlybudget.model.Category;
import edu.wwu.onlybudget.model.DatabaseManager;

public class CategoryListActivity extends AppCompatActivity {
    DatabaseManager dbManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_list);

        dbManager = new DatabaseManager(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // go to add activity
                Intent addIntent = new Intent(view.getContext(), AddCategoryActivity.class);
                startActivity(addIntent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        populateCategoryList();
    }



    // get categories from the DB and put them on screen
    public void populateCategoryList() {
        int imgSize = 100;
        LinearLayout layout = findViewById(R.id.categories_list);
        Iterable<Category> categories = Category.loadMany(dbManager.getReadableDatabase());
        layout.removeAllViewsInLayout();
        if (categories != null) {
            for (Category cat : categories) {
                if (!cat.isDeleted()) {
                    // linear layout for row
                    RelativeLayout relativeParent = new RelativeLayout(this);
                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                    params.topMargin = 35;
                    relativeParent.setLayoutParams(params);
                    // category name
                    TextView nameText = new TextView(this);
                    nameText.setTextSize(23.0f);
                    // images
                    ImageView editImg = new ImageView(this);
                    ImageView deleteImg = new ImageView(this);
                    // set image resources
                    editImg.setImageResource(R.drawable.ic_pencil_black_24dp);
                    deleteImg.setImageResource(R.drawable.ic_delete_red_24dp);
                    // set parameters right-most icon
                    RelativeLayout.LayoutParams imgParams = new RelativeLayout.LayoutParams(imgSize, imgSize);
                    imgParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);

                    deleteImg.setLayoutParams(imgParams);

                    // set parameters for left-most icon
                    RelativeLayout.LayoutParams imgParamsLeft = new RelativeLayout.LayoutParams(imgSize, imgSize);
                    imgParamsLeft.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);

                    imgParamsLeft.rightMargin = 200;
                    editImg.setLayoutParams(imgParamsLeft);

                    editImg.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent editIntent = new Intent(v.getContext(), EditCategoryActivity.class);
                            editIntent.putExtra("categoryId", cat.getId());
                            startActivity(editIntent);
                        }
                    });

                    deleteImg.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            cat.delete(dbManager.getWritableDatabase());
                            ((CategoryListActivity) v.getContext()).populateCategoryList();
                            Log.w("cat info", cat.toString());
                        }
                    });

                    nameText.setText(cat.getName());
                    relativeParent.addView(nameText);
                    relativeParent.addView(editImg);
                    relativeParent.addView(deleteImg);
                    layout.addView(relativeParent);
                }
            }
        }
    }
}
