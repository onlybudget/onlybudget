package edu.wwu.onlybudget.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Date;

import edu.wwu.onlybudget.R;
import edu.wwu.onlybudget.controller.ExpenseController;

public class ExpenseActivity extends AppCompatActivity {
    public static final String INITIAL_EXPENSE_ID = "__EXPENSE_ACTIVITY__INITIAL_EXPENSE_ID__";

    private ExpenseController controller;

    private TextView dateView;

    private Spinner categorySelector;

    private EditText priceEdit;
    private EditText frivolousEdit;
    private SeekBar frivolousSeek;
    private EditText taxEdit;
    private SeekBar taxSeek;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expense);
        onCreate_processIntent();
        onCreate_findViews();
        onCreate_wireEvents();
        onCreate_finalizeUi();
    }

    private void onCreate_processIntent() {
        final Intent intent = getIntent();
        if (intent.hasExtra(INITIAL_EXPENSE_ID)) {
            controller = new ExpenseController(this, intent.getLongExtra(INITIAL_EXPENSE_ID, -1));
        } else {
            controller = new ExpenseController(this);
        }
    }

    private void onCreate_findViews() {
        dateView = findViewById(R.id.display_date);

        priceEdit = findViewById(R.id.expense_price_input);

        frivolousSeek = findViewById(R.id.seek_bar_frivolous);
        frivolousEdit = findViewById(R.id.expense_frivolous_input);

        taxSeek = findViewById(R.id.seek_bar_taxes);
        taxEdit = findViewById(R.id.expense_tax_input);

        categorySelector = findViewById(R.id.spinner_category);
    }

    private void onCreate_wireEvents() {
        // Value change to UI propagation
        controller.setDateConsumer(this::dateChanged);
        controller.setTaxChangedConsumer(this::taxUpdated);
        controller.setFrivolousChangedConsumer(this::frivolousUpdated);
        controller.setPriceChangedConsumer(this::priceUpdated);

        // Button events
        final Button changeDate = findViewById(R.id.button_change_date);
        final Button saveExpense = findViewById(R.id.button_save_expense);

        changeDate.setOnClickListener(controller::modifyDateClicked);
        saveExpense.setOnClickListener(controller::saveExpense);

        // EditText events
        final EditText nameInput = findViewById(R.id.expense_name_input);
        nameInput.setText(controller.getExpense().getDescription());

        nameInput.addTextChangedListener(controller.getNameWatcher());
        priceEdit.addTextChangedListener(controller.getPriceWatcher());
        frivolousEdit.addTextChangedListener(controller.getFrivolousnessWatcher());
        taxEdit.addTextChangedListener(controller.getTaxWatcher());

        // Seek events
        frivolousSeek.setOnSeekBarChangeListener(controller.getFrivolousnessSeekListener());
        taxSeek.setOnSeekBarChangeListener(controller.getTaxSeekListener());

        // Spinner events
        categorySelector.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(final AdapterView<?> adapterView, final View view, final int position, final long id) {
                        controller.categorySelected(id);
                    }

                    @Override
                    public void onNothingSelected(final AdapterView<?> adapterView) {
                    }
                }
        );
    }

    private void onCreate_finalizeUi() {
        long catId = controller.getExpense().getCategoryId();
        // give catId to include a deleted category
        final CategorySpinnerAdapter adapter = new CategorySpinnerAdapter(this, catId);
        categorySelector.setAdapter(adapter);
        // get location of the category in the list
        int select = 0;
        for (; select < adapter.getCount(); select++) {
            if (adapter.getItemId(select) == catId) {
                break;
            }
        }
        if (select == adapter.getCount()) {
            select = 0;
        }
        //categorySelector.setSelection((int) controller.getExpense().getCategoryId() - 1);
        categorySelector.setSelection(select);
    }

    private void dateChanged(final Date date) {
        dateView.setText(DefaultDateFormatter.format(date));
    }

    private void maybeUpdateEditText(final EditText toUpdate, final double value) {
        final String doesDisplay = toUpdate.getText().toString();
        final String shouldDisplay = Double.toString(Math.round(value * 100L) / 100.0);
        if (!doesDisplay.equals(shouldDisplay)) {
            toUpdate.setText(shouldDisplay);
        }
    }

    private void priceUpdated(final double price) {
        maybeUpdateEditText(priceEdit, price);
        frivolousSeek.setMax((int) price * 100);
        taxSeek.setMax((int) price * 100);
    }

    private void frivolousUpdated(final double frivolous) {
        maybeUpdateEditText(frivolousEdit, frivolous);
        frivolousSeek.setProgress((int) frivolous * 100);
    }

    private void taxUpdated(final double tax) {
        maybeUpdateEditText(taxEdit, tax);
        taxSeek.setProgress((int) tax * 100);
    }

    public void showDetails(View v) {
        // set details visible
        findViewById(R.id.layout_frivolous).setVisibility(View.VISIBLE);
        findViewById(R.id.seek_bar_frivolous).setVisibility(View.VISIBLE);
        findViewById(R.id.layout_tax).setVisibility(View.VISIBLE);
        findViewById(R.id.seek_bar_taxes).setVisibility(View.VISIBLE);
        findViewById(R.id.layout_date).setVisibility(View.VISIBLE);
        // set button to gone
        findViewById(R.id.button_show_detail).setVisibility(View.GONE);
    }
}
