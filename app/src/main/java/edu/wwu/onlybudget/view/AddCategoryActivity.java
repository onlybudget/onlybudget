package edu.wwu.onlybudget.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import edu.wwu.onlybudget.R;
import edu.wwu.onlybudget.model.Category;
import edu.wwu.onlybudget.model.DatabaseManager;

public class AddCategoryActivity extends AppCompatActivity {
    DatabaseManager dbManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_category);
        dbManager = new DatabaseManager(this);
    }

    // add the category
    public void addNew(View v) {
        TextView tv = findViewById(R.id.text_new_category);
        String newCatName = tv.getText().toString();
        if (!newCatName.equals("")) {
            Category newCat = new Category(newCatName);
            dbManager.store(newCat);
        }
        this.finish();
    }

    public void cancel(View v) {
        this.finish();
    }
}
