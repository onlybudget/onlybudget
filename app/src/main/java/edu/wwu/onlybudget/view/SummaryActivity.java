package edu.wwu.onlybudget.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import java.util.Locale;
import java.util.function.Consumer;

import edu.wwu.onlybudget.R;
import edu.wwu.onlybudget.controller.SummaryController;

public class SummaryActivity extends AppCompatActivity {
    private SummaryController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);

        controller = new SummaryController(this);

        controller.setTotalSpendingChangedListener(
                updateAmountFunc(findViewById(R.id.total_amount_display)));
        controller.setVitalSpendingChangedListener(
                updateAmountFunc(findViewById(R.id.vital_amount_display)));
        controller.setFrivolousSpendingChangedListener(
                updateAmountFunc(findViewById(R.id.frivolous_amount_display)));

        controller.setPieChartSupplier(() -> findViewById(R.id.pie_chart));

        controller.loadExpenses();
    }

    private Consumer<Double> updateAmountFunc(final TextView forView) {
        return (d) -> forView.setText(String.format(Locale.getDefault(), "$%.2f", d));
    }
}
