package edu.wwu.onlybudget.view;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import edu.wwu.onlybudget.model.Category;
import edu.wwu.onlybudget.model.DatabaseManager;

public final class CategorySpinnerAdapter extends BaseAdapter implements SpinnerAdapter {
    private final Context context;

    private final List<Category> categories;

    public CategorySpinnerAdapter(final Context context, long catId) {
        this.context = context;
        this.categories = new ArrayList<>();

        final DatabaseManager dbManager = new DatabaseManager(context);
        final Iterable<Category> dbResult = Category.loadMany(dbManager.getReadableDatabase());
        if (dbResult != null) {
            for (Category cat : dbResult) {
                if (!cat.isDeleted() || cat.getId() == catId) {
                    categories.add(cat);
                }
            }
        }
    }

    @Override
    public int getCount() {
        return categories.size();
    }

    @Override
    public Object getItem(int i) {
        return categories.get(i);
    }

    @Override
    public long getItemId(int i) {
        return categories.get(i).getId();
    }

    @Override
    public View getView(final int position, final View existing, final ViewGroup parent) {
        final TextView view;
        if (existing != null && existing instanceof TextView) {
            view = (TextView) existing;
        } else {
            view = new TextView(context);
        }
        view.setTextSize(20);
        view.setPadding(16, 16, 16, 16);

        final Category category = categories.get(position);
        view.setText(category.getName());
        view.setTag(category.getId());
        return view;
    }
}
