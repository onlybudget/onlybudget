package edu.wwu.onlybudget.view;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;

import java.util.Calendar;
import java.util.Date;


public final class DatePickerFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {
    private static final String INITIAL_TIME_KEY = "__INITIAL_TIME__";

    private Date date = new Date();

    private DatePickerDialog.OnDateSetListener onDateSetListener = null;

    public static DatePickerFragment newInstance(final Date date) {
        final DatePickerFragment fragment = new DatePickerFragment();
        final Bundle args = new Bundle();
        args.putLong(INITIAL_TIME_KEY, date.getTime());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void setArguments(final Bundle arguments) {
        if (arguments == null) {
            return;
        }

        if (arguments.containsKey(INITIAL_TIME_KEY)) {
            date = new Date(arguments.getLong(INITIAL_TIME_KEY));
        }
    }

    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            setArguments(savedInstanceState);
        }
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return new DatePickerDialog(getActivity(), this,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));
    }

    public void setOnDateSetListener(final DatePickerDialog.OnDateSetListener listener) {
        this.onDateSetListener = listener;
    }

    @Override
    public void onDateSet(final DatePicker view, final int year, final int month, final int dayOfMonth) {
        if (onDateSetListener != null) {
            onDateSetListener.onDateSet(view, year, month, dayOfMonth);
        }
    }
}
