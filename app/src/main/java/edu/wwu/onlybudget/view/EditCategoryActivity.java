package edu.wwu.onlybudget.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import edu.wwu.onlybudget.R;
import edu.wwu.onlybudget.model.Category;
import edu.wwu.onlybudget.model.DatabaseManager;

public class EditCategoryActivity extends AppCompatActivity {
    DatabaseManager dbManager;
    long catId;
    Category cat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_category);
        dbManager = new DatabaseManager(this);
        Bundle extras = getIntent().getExtras();
        catId = extras.getLong("categoryId");
        // load category name
        if (catId > 0) {
            cat = new Category(catId);
            cat.load(dbManager.getReadableDatabase());
            ((EditText) findViewById(R.id.text_edit_category)).setText(cat.getName());
        } else {
            finish();
        }
    }

    public void cancel(View v) {
        this.finish();
    }

    public void editItem(View v) {
        String newName = ((TextView) findViewById(R.id.text_edit_category)).getText().toString();
        cat.setName(newName);
        cat.update(dbManager.getWritableDatabase());
        this.finish();
    }
}
