package edu.wwu.onlybudget.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import edu.wwu.onlybudget.R;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
    }

    // show categories
    public void editCategories(View v) {
        Intent editIntent = new Intent(this, CategoryListActivity.class);
        startActivity(editIntent);
    }

    public void openKeyboard(final View v) {
        final Intent intent = new Intent(this, InputCurrencyActivity.class);
        startActivity(intent);
    }
}
