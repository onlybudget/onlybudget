package edu.wwu.onlybudget.controller;

import android.app.Activity;

public abstract class ActivityController {
    protected final Activity activity;

    protected ActivityController(final Activity activity) {
        this.activity = activity;
    }
}
