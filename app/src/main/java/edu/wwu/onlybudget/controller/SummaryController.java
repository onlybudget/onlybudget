package edu.wwu.onlybudget.controller;

import android.app.Activity;
import android.content.res.TypedArray;
import android.util.SparseArray;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Consumer;
import java.util.function.Supplier;

import edu.wwu.onlybudget.R;
import edu.wwu.onlybudget.model.DatabaseManager;
import edu.wwu.onlybudget.model.Expense;
import edu.wwu.onlybudget.util.Cubehelix;
import edu.wwu.onlybudget.util.Lambda;

public final class SummaryController extends ActivityController {
    private final DatabaseManager dbManager;

    private Consumer<Double> totalSpendingChangedListener;
    private Consumer<Double> vitalSpendingChangedListener;
    private Consumer<Double> frivolousSpendingChangedListener;

    private Supplier<PieChart> pieChartSupplier;

    private Iterable<Expense> expenses;

    public SummaryController(final Activity activity) {
        super(activity);

        dbManager = new DatabaseManager(activity);

        totalSpendingChangedListener = Lambda::consume;
        vitalSpendingChangedListener = Lambda::consume;
        frivolousSpendingChangedListener = Lambda::consume;

        pieChartSupplier = Lambda::supplyNull;

        expenses = new ArrayList<>();
    }

    public void setTotalSpendingChangedListener(final Consumer<Double> listener) {
        this.totalSpendingChangedListener = listener;
    }

    public void setVitalSpendingChangedListener(final Consumer<Double> listener) {
        this.vitalSpendingChangedListener = listener;
    }

    public void setFrivolousSpendingChangedListener(final Consumer<Double> listener) {
        this.frivolousSpendingChangedListener = listener;
    }

    public void setPieChartSupplier(final Supplier<PieChart> supplier) {
        this.pieChartSupplier = supplier;
    }

    public void loadExpenses() {
        expenses = dbManager.loadAllExpenses();
        updateSpending();
        updateChart();
    }

    private void updateSpending() {
        double total = 0, frivolous = 0;

        for (final Expense expense : expenses) {
            total += expense.getAmount();
            frivolous += expense.getFrivolous();
        }

        totalSpendingChangedListener.accept(total);
        vitalSpendingChangedListener.accept(total - frivolous);
        frivolousSpendingChangedListener.accept(frivolous);
    }

    private void updateChart() {
        final PieChart chart = pieChartSupplier.get();
        if (chart == null) {
            return;
        }

        final Set<Integer> keys = new TreeSet<>();
        final SparseArray<Double> vitalBuckets = new SparseArray<>();
        final SparseArray<Double> frivolousBuckets = new SparseArray<>();
        for (final Expense expense : expenses) {
            final double frivolous = expense.getFrivolous();
            final double vital = expense.getAmount() - frivolous;
            int key = (int) expense.getCategoryId();

            if (keys.add(key)) {
                vitalBuckets.put(key, vital);
                frivolousBuckets.put(key, frivolous);
            } else {
                vitalBuckets.put(key, vital + vitalBuckets.get(key));
                frivolousBuckets.put(key, frivolous + frivolousBuckets.get(key));
            }
        }

        final int numBuckets = vitalBuckets.size();
        final double interpolationAmount = 1.0 / numBuckets;

        final List<PieEntry> entries = new ArrayList<>();
        final List<Integer> entryColors = new ArrayList<>();
        final List<LegendEntry> legendEntries = new ArrayList<>();
        double currentInterpolation = 0.0;
        for (final int key : keys) {
            final String label = dbManager.loadCategory(key).getName();
            final int color = Cubehelix.interpolate(currentInterpolation);

            final double v = vitalBuckets.get(key);
            if (v != 0) {
                entries.add(new PieEntry((float) v));
                entryColors.add(color);

                final LegendEntry legendEntry = new LegendEntry();
                legendEntry.label = label + " (vital)";
                legendEntry.formColor = color;
                legendEntries.add(legendEntry);
            }

            final double f = frivolousBuckets.get(key);
            if (f != 0) {
                final int darkColor = color & 0xB0FFFFFF;
                entries.add(new PieEntry((float) f));
                entryColors.add(darkColor);

                final LegendEntry legendEntry = new LegendEntry();
                legendEntry.label = label + " (frivolous)";
                legendEntry.formColor = darkColor;
                legendEntries.add(legendEntry);
            }
            currentInterpolation += interpolationAmount;
        }

        final PieDataSet dataSet = new PieDataSet(entries, "Expenses");
        dataSet.setColors(entryColors);

        final PieData data = new PieData(dataSet);
        data.setValueTextSize(12f);
        data.setValueFormatter(
                (value, _0, _1, _2) -> String.format(Locale.getDefault(), "$%.2f", value));

        chart.setHoleRadius(15f);
        chart.setTransparentCircleRadius(95f);
        chart.setData(data);

        final Legend legend = chart.getLegend();
        legend.setTextSize(16f);
        legend.setCustom(legendEntries);
        legend.setWordWrapEnabled(true);

        final Description description = new Description();
        description.setText("");
        chart.setDescription(description);

        chart.invalidate();
    }
}
