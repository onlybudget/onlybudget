package edu.wwu.onlybudget.controller;

import android.app.Activity;
import android.text.TextWatcher;
import android.view.View;
import android.widget.SeekBar;

import java.util.Calendar;
import java.util.Date;
import java.util.function.Consumer;
import java.util.function.DoubleConsumer;
import java.util.function.IntConsumer;

import edu.wwu.onlybudget.model.DatabaseManager;
import edu.wwu.onlybudget.model.Expense;
import edu.wwu.onlybudget.view.DatePickerFragment;

import edu.wwu.onlybudget.R;

public final class ExpenseController extends ActivityController {
    private static final String FRAGMENT_TAG = "datePicker";

    private final DatabaseManager dbManager;

    private final Expense expense;

    // Consumers are used to fire events back to the UI when values change.
    private Consumer<Date> dateConsumer;
    private DoubleConsumer priceChangedConsumer;
    private DoubleConsumer frivolousChangedConsumer;
    private DoubleConsumer taxChangedConsumer;

    public ExpenseController(final Activity activity) {
        this(activity, -1);
    }

    public ExpenseController(final Activity activity, final long expenseId) {
        super(activity);
        dbManager = new DatabaseManager(activity);
        expense = expenseId == -1
                ? new Expense()
                : dbManager.loadExpense(expenseId);
    }

    public Expense getExpense() {
        return expense;
    }

    public void setDateConsumer(final Consumer<Date> dateConsumer) {
        this.dateConsumer = dateConsumer;
        dateConsumer.accept(expense.getDate());
    }

    public void setPriceChangedConsumer(final DoubleConsumer consumer) {
        this.priceChangedConsumer = consumer;
        priceChangedConsumer.accept(expense.getAmount());
    }

    public void setFrivolousChangedConsumer(final DoubleConsumer consumer) {
        this.frivolousChangedConsumer = consumer;
        frivolousChangedConsumer.accept(expense.getAmount());
    }

    public void setTaxChangedConsumer(final DoubleConsumer consumer) {
        this.taxChangedConsumer = consumer;
        taxChangedConsumer.accept(expense.getAmount());
    }

    private void changePrice(final double price) {
        final double previousAmount = expense.getAmount();
        expense.setAmount(price);
        if (priceChangedConsumer != null) {
            priceChangedConsumer.accept(price);
        }

        if (price != 0 && previousAmount != 0) {
            final double fRatio = expense.getFrivolous() / previousAmount;
            final double tRatio = expense.getTax() / previousAmount;

            final double nFrivolous = fRatio * price;
            final double nTax = tRatio * price;

            changeFrivolous(nFrivolous);
            changeTax(nTax);
        }
    }

    private void changeFrivolous(double frivolous) {
        frivolous = Math.max(0, frivolous);
        frivolous = Math.min(frivolous, expense.getAmount());
        expense.setFrivolous(frivolous);
        if (frivolous + expense.getTax() > expense.getAmount()) {
            changeTax(expense.getAmount() - frivolous);
        }
        if (frivolousChangedConsumer != null) {
            frivolousChangedConsumer.accept(frivolous);
        }
    }

    private void changeTax(double tax) {
        tax = Math.max(0, tax);
        tax = Math.min(tax, expense.getAmount());
        expense.setTax(tax);
        if (tax + expense.getFrivolous() > expense.getAmount()) {
            changeFrivolous(expense.getAmount() - tax);
        }
        if (taxChangedConsumer != null) {
            taxChangedConsumer.accept(tax);
        }
    }

    public void modifyDateClicked(final View v) {
        final DatePickerFragment fragment = DatePickerFragment.newInstance(expense.getDate());
        fragment.setOnDateSetListener((view, year, month, dayOfMonth) -> {
            final Calendar calendar = Calendar.getInstance();
            calendar.set(year, month, dayOfMonth);
            expense.setDate(calendar.getTime());
            dateConsumer.accept(expense.getDate());
        });
        fragment.show(activity.getFragmentManager(), FRAGMENT_TAG);
    }

    public void saveExpense(final View v) {
        final Expense expense = this.expense;
        dbManager.store(expense);
        activity.finish();
    }

    public TextWatcher getNameWatcher() {
        return new SimpleWatcher(expense::setDescription);
    }

    public TextWatcher getPriceWatcher() {
        return new SimpleWatcher(doubleParsingConsumer(this::changePrice));
    }

    public TextWatcher getFrivolousnessWatcher() {
        return new SimpleWatcher(doubleParsingConsumer(this::changeFrivolous));
    }

    public SeekBar.OnSeekBarChangeListener getFrivolousnessSeekListener() {
        return new SimpleSeekListener(seekTranslatingConsumer(this::changeFrivolous));
    }

    public TextWatcher getTaxWatcher() {
        return new SimpleWatcher(doubleParsingConsumer(this::changeTax));
    }

    public SeekBar.OnSeekBarChangeListener getTaxSeekListener() {
        return new SimpleSeekListener(seekTranslatingConsumer(this::changeTax));
    }

    private Consumer<String> doubleParsingConsumer(final DoubleConsumer setter) {
        return (s) -> {
            try {
                setter.accept(Double.valueOf(s));
            } catch (final Exception e) {
                setter.accept(0.00);
            }
        };
    }

    private IntConsumer seekTranslatingConsumer(final DoubleConsumer setter) {
        return (i) -> {
            setter.accept(i / 100.0);
        };
    }

    public void categorySelected(final long id) {
        expense.setCategoryId(id);
    }
}
