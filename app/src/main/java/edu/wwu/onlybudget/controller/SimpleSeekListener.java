package edu.wwu.onlybudget.controller;

import android.widget.SeekBar;

import java.util.function.IntConsumer;

public class SimpleSeekListener implements SeekBar.OnSeekBarChangeListener {
    private IntConsumer consumer;

    public SimpleSeekListener(final IntConsumer consumer) {
        this.consumer = consumer;
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (fromUser && consumer != null) {
            consumer.accept(progress);
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}
