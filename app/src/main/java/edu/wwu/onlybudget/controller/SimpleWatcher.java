package edu.wwu.onlybudget.controller;

import android.text.Editable;
import android.text.TextWatcher;

import java.util.function.Consumer;

public final class SimpleWatcher implements TextWatcher {
    private Consumer<String> beforeTextChangedConsumer;
    private Consumer<String> onTextChangedConsumer;
    private Consumer<Editable> afterTextChangedConsumer;

    public SimpleWatcher() {
        this((o) -> {
        }, (o) -> {
        }, (o) -> {
        });
    }

    public SimpleWatcher(final Consumer<String> consumer) {
        this((o) -> {
        }, consumer, (o) -> {
        });
    }

    public SimpleWatcher(final Consumer<String> before, final Consumer<String> on, final Consumer<Editable> after) {
        this.beforeTextChangedConsumer = before;
        this.onTextChangedConsumer = on;
        this.afterTextChangedConsumer = after;
    }

    public void setOnTextChangedConsumer(final Consumer<String> consumer) {
        this.onTextChangedConsumer = consumer;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        beforeTextChangedConsumer.accept(s.toString());
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        onTextChangedConsumer.accept(s.toString());
    }

    @Override
    public void afterTextChanged(Editable s) {
        afterTextChangedConsumer.accept(s);
    }
}
